#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <cstdlib>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <trajectory_control/Command.h>
#include <std_msgs/UInt8.h>
#include "dji_sdk/Gimbal.h"

template <typename T> double sgn(T val)
{
    return (T(0) < val) - (val < T(0));
}

ros::Subscriber velcmd_subscriber,req_auth_subscriber, gimbalcmd_subscriber, gimbalang_subscriber;
DJIDrone* drone;
dji_sdk::Gimbal gimbal;

bool got_waypoints=false;
bool alreadyAuth = false;
double speed_max;
using namespace std;

void send_velocity_cmd_cb(const trajectory_control::Command cmd)
{
    ROS_INFO_ONCE("Recieving vel cmds\n");
    drone->attitude_control(HORIZ_VEL|VERT_VEL|YAW_RATE|HORIZ_GND|YAW_GND, cmd.velocity.x, cmd.velocity.y, -cmd.velocity.z, cmd.headingrate*180/M_PI);
}

void send_auth_req(const std_msgs::UInt8 msg)
{
    ROS_INFO_THROTTLE(1,"SDK Permissions %d\n",msg.data);
    if(msg.data != 1 && !alreadyAuth)
    {
        drone->request_sdk_permission_control();
        alreadyAuth = false;
        sleep(1);
    }
    else if(msg.data == 1)
        alreadyAuth = true;
}

void send_gimbal_cmd_cb(const dji_sdk::Gimbal &cmd)
{
    ROS_INFO_ONCE("Recieving gimbal angle command\n");

    double roll_rate, pitch_rate, yaw_rate;
    
    roll_rate = sgn(cmd.roll - gimbal.roll) * speed_max;
    pitch_rate = sgn(cmd.pitch - gimbal.pitch) * speed_max;
    yaw_rate = sgn(cmd.yaw - gimbal.yaw) * speed_max;

    drone->gimbal_speed_control((int)roll_rate, (int)pitch_rate, (int)yaw_rate);
  
}

void gimbal_angle_cb(const dji_sdk::Gimbal &msg)
{
    gimbal.roll = msg.roll;
    gimbal.pitch = msg.pitch;
    gimbal.yaw = msg.yaw;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "dji_interface");
    ROS_INFO("sdk_service_client_test");
    ros::NodeHandle n;
    ros::NodeHandle n2;
    drone = new DJIDrone(n);
    bool success = false;

    velcmd_subscriber= n2.subscribe("/trajectory_control/command", 50 , send_velocity_cmd_cb);
    req_auth_subscriber= n2.subscribe("/dji_sdk/sdk_permission", 50 , send_auth_req);
    gimbalcmd_subscriber= n.subscribe("/dji_sdk/gimbal_angle", 50, send_gimbal_cmd_cb);
    gimbalang_subscriber= n.subscribe("/dji_sdk/gimbal", 50, gimbal_angle_cb);
    n.param("/dji_interface/gimbal_speed_max", speed_max, 50.0);
    
    ros::Rate r(50);
    while(ros::ok())
    {
//        if(!success)
//        {
//            success = drone->request_sdk_permission_control();
//            ROS_INFO("Request Control Successful: %d",success);
 //           sleep(2);
 //       }
        r.sleep();
        ros::spinOnce();
    }
    n.shutdown();
    n2.shutdown();
    ROS_INFO("Request Release Control Successful: %d",drone->release_sdk_permission_control());
    return 0;
}

