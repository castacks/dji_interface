#include <ros/ros.h>
#include "SDK.h"
#include <cstdlib>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float64.h>
#include <iostream>
#include <tf/tf.h>
#include <trajectory_control/Command.h>




#define HORIZ_ATT 0x00
#define HORIZ_VEL 0x40
#define HORIZ_POS 0x80
#define VERT_VEL 0x00
#define VERT_POS 0x10
#define VERT_TRU 0x20
#define YAW_ANG 0x00
#define YAW_RATE 0x08
#define HORIZ_GND 0x00
#define HORIZ_BODY 0x02
#define YAW_GND 0x00
#define YAW_BODY 0x01

dji_ros::control_manager 	srv_control;
dji_ros::action 			srv_action;
dji_ros::attitude 			srv_attitude;
dji_ros::camera_action		srv_camera;
dji_ros::gimbal_angle 		srv_gimbal_angle;
dji_ros::gimbal_speed 		srv_gimbal_speed;



ros::ServiceClient drone_control_manager, drone_action_client, drone_attitude_client;

bool flag=false;


class DJI_client

{
	public:
		DJI_client(bool flag);
		void send_command(int command_action);
		void send_trajectory(const nav_msgs::Odometry way_points);
        void send_velocity_cmd(const trajectory_control::Command cmd);
        trajectory_control::Command cmd;
		~DJI_client();
};


DJI_client::DJI_client(bool flag)
{
		if (!flag)
	{
		flag=true;
		printf("Welcome to DJI Matrice Command Control \n");
     	printf("......Taking Over Control........ \n");
        srv_control.request.control_ability=1;
        drone_control_manager.call(srv_control);
	}
}



void DJI_client::send_command(int command_action)
{
   if(command_action==5)
   {
    /* take off */

    printf("\n TakeOff \n");
    srv_action.request.action=4;
	drone_action_client.call(srv_action);
   }
   else if (command_action==10)
   {
    /* landing*/

    printf("\n landing \n");
    srv_action.request.action=6;
	drone_action_client.call(srv_action);   	
   }
   else if(command_action==20)
   {
    /* go home*/

    printf("\n Go Home \n");
    srv_action.request.action=0;
	drone_action_client.call(srv_action);   	
   }
   else
    {
    }

}



void DJI_client::send_trajectory(const nav_msgs::Odometry way_points)
{
	printf("Recieving way points\n");
    
    srv_attitude.request.flag = HORIZ_VEL|VERT_VEL|YAW_ANG|HORIZ_BODY|YAW_BODY;
    srv_attitude.request.x = way_points.twist.twist.linear.x;
    srv_attitude.request.y = way_points.twist.twist.linear.y;
    srv_attitude.request.z = way_points.twist.twist.linear.z;    
    srv_attitude.request.yaw = way_points.twist.twist.angular.z;
//    ROS_INFO("%f",tf::getYaw(way_points.pose.pose.orientation));
//    srv_attitude.request.yaw = tf::getYaw(way_points.pose.pose.orientation);
    drone_attitude_client.call(srv_attitude); 

}

void DJI_client::send_velocity_cmd(const trajectory_control::Command cmd)
{
    ROS_INFO_ONCE("Recieving vel cmds\n");

    srv_attitude.request.flag = HORIZ_VEL|VERT_VEL|YAW_ANG|HORIZ_GND|YAW_GND;
    srv_attitude.request.x = cmd.velocity.x;
    srv_attitude.request.y = cmd.velocity.y;
    srv_attitude.request.z = -cmd.velocity.z;
    srv_attitude.request.yaw = cmd.heading*180/M_PI;
//    ROS_INFO("Yaw: %f",cmd.heading);
//    ROS_INFO("%f",tf::getYaw(way_points.pose.pose.orientation));
//    srv_attitude.request.yaw = tf::getYaw(way_points.pose.pose.orientation);
    drone_attitude_client.call(srv_attitude);

}

DJI_client::~DJI_client()
{

  printf(" ......Releasing Control........ \n");
  srv_control.request.control_ability=0;
  drone_control_manager.call(srv_control);		

}


